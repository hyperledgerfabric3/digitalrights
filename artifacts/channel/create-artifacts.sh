# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF-UNINETWORK_0/fabric-samples/bin" ] ; then
    PATH="/workspaces/HLF-UNINETWORK_0/fabric-samples/bin:$PATH"
fi

# Delete existing artifacts
rm -rf ./crypto-config
rm digital-rights-genesis.block digital-rights-channel.tx
#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/


# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD

#generating genesis block
configtxgen -outputBlock digital-rights-genesis.block -profile DigitalRightsOrdererGenesis -channelID digitalrightschannel

# Create the channel digitalrights
configtxgen -outputCreateChannelTx digital-rights-channel.tx -profile DigitalRightsChannel -channelID digitalrightschannel

#for generrating anchor peer of distributor
configtxgen -outputAnchorPeersUpdate DistributorsAnchors.tx -profile DigitalRightsChannel -channelID digitalrightschannel -asOrg DistributorsMSP

#for generating anchor peer of content creator
configtxgen -outputAnchorPeersUpdate ContentCreatorsAnchors.tx -profile DigitalRightsChannel -channelID digitalrightschannel -asOrg ContentCreatorsMSP


