# Set the environment variables for overriding the config in core.yaml
export FABRIC_LOGGING_SPEC=INFO

# Specify the MSP paths for the peers' identities
export CONTENT_CREATORS_MSPCONFIGPATH=$CONFIG_DIRECTORY/crypto-config/peerOrganizations/contentcreators.com/peers/peer0.contentcreators.com/msp

# Set the MSP config path for ContentCreators organization
export CORE_PEER_MSPCONFIGPATH=$CONTENT_CREATORS_MSPCONFIGPATH
# Launch the node for ContentCreators organization
peer node start 

